<?php

namespace App\Http\Controllers;

use App\Datalogs;
use Illuminate\Http\Request;

class DatalogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Datalogs  $datalogs
     * @return \Illuminate\Http\Response
     */
    public function show(Datalogs $datalogs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Datalogs  $datalogs
     * @return \Illuminate\Http\Response
     */
    public function edit(Datalogs $datalogs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Datalogs  $datalogs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Datalogs $datalogs)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Datalogs  $datalogs
     * @return \Illuminate\Http\Response
     */
    public function destroy(Datalogs $datalogs)
    {
        //
    }
}
