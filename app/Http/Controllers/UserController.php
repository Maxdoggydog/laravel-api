<?php

namespace App\Http\Controllers;

use App\User;
use App\Action;
use Illuminate\Http\Request;
use JWTAuth;
use App\Newsfeed;
use AddUserParams as addUserParams;

class UserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function show(User $user)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function edit(User $user)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, User $user)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function destroy(User $user)
  {
      //
  }

  /**
   * Get the newsfeed from the user.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function getnewsfeeduser(Request $request)
  {
    $user = JWTAuth::toUser($request->token);
    return response()->json(['newsfeed' => $user->newsfeed->getId()]);
  }

  /**
   * Check if the newsfeed belongs to the user.
   *
   * @param  \App\User  $user
   * @return \Illuminate\Http\Response
   */
  public function newsfeedbelongstouser(Request $request)
  {
    $user = JWTAuth::toUser($request->token);
    $newsfeed_id = $request['newsfeed'];
    return response()->json(['belongs' => $user->isInNewfeeds($newsfeed_id)]);
  }

  /**
   * Get info of user
   * Change params to return it to the good format
   */
  protected function getAuthUser(Request $request){
    $user = JWTAuth::toUser($request->token);
    $action = Action::find($user->id);
    $user = addUserParams::addUserParams($user, $action);
    return response()->json(['result' => $user]);
  }

  /**
   * Return users depending of the newsfeed id ou team id
   */
  public function users (Request $request)
  {
    $newsfeed_id = intval($request['newsfeed_id']);
    $team_id = intval($request['team_id']);
    $users = User::where([])->get(); // Get all user
    $res = [];

    if($team_id){
      foreach ($users as $user) {
        // If the user got the team id in database
        // Add him to the array
        if($user->isInTeams($team_id)){
          // Add the gamification in user
          $action = Action::find($user->id);
          $user = addUserParams::addUserParams($user, $action);
          array_push($res, $user);
        }
      }
    }else{
      foreach ($users as $user) {
        if($user->isInNewfeeds($newsfeed_id)){
          $action = Action::find($user->id);
          $user = addUserParams::addUserParams($user, $action);
          array_push($res, $user);
        }
      }
    }
    return response()->json(['result' => $res]);
  }
}
