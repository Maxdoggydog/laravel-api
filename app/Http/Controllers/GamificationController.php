<?php

namespace App\Http\Controllers;

use App\Gamification;
use Illuminate\Http\Request;
use Ini_gamification as ini_gamification;

class GamificationController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
      //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      //
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Gamification  $gamification
   * @return \Illuminate\Http\Response
   */
  public function show(Gamification $gamification)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Gamification  $gamification
   * @return \Illuminate\Http\Response
   */
  public function edit(Gamification $gamification)
  {
      //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  \App\Gamification  $gamification
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Gamification $gamification)
  {
      //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Gamification  $gamification
   * @return \Illuminate\Http\Response
   */
  public function destroy(Gamification $gamification)
  {
      //
  }

  /**
   * Get json for gamification.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function get(Request $request)
  {
    $gamification = Gamification::where('name', 'dev')->first();
    if(!$gamification){
      $gamification = Gamification::create([
        'name' => 'dev',
        'json' => ini_gamification::initJsonGamification()
      ]);
    }
    $gamification->json = $gamification->getJson();
    return response()->json(['result' => $gamification]);
  }
}
