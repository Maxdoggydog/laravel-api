<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use JWTAuth;
use App\User;
use App\Action;
use App\Newsfeed;
use JWTAuthException;
use AddUserParams as addUserParams;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Log the user
     */
    protected function loginUser(Request $request){
        $credentials = $request->only('email', 'password');
        $token = null;
        try {
           if (!$token = JWTAuth::attempt($credentials)) {
            return response()->json(['invalid_email_or_password'], 422);
           }
        } catch (JWTAuthException $e) {
            return response()->json(['failed_to_create_token'], 500);
        }
        // Return the user information
        $user = JWTAuth::toUser(compact('token')['token']);
        $action = Action::find($user->id);
        $user = addUserParams::addUserParams($user, $action);
        return response()->json(array_merge(compact('token'), ['newsfeed' => $user->newsfeed->getId(), 'user' => $user]));
    }

    /**
     * Get info of user
     */
    protected function getAuthUser(Request $request){
        $user = JWTAuth::toUser($request->token);
        $action = Action::find($user->id);
        $user = addUserParams::addUserParams($user, $action);
        return response()->json(['result' => $user]);
    }

    /**
     * User login out
     */
    protected function logout(Request $request){
        $token = null;
        JWTAuth::invalidate($token);
        return;
    }
}
