<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Newsfeed;
use App\Action;
use App\Gamification;
use App\Image;
use App\Providers\GamificationServiceProvider;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use JWTAuth;
use Ini_gamification as ini_gamification;
use AddUserParams as addUserParams;
use App\Http\Requests\UploadRequest;

class RegisterController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Register Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles the registration of new users as well as their
  | validation and creation. By default this controller uses a trait to
  | provide this functionality without requiring any additional code.
  |
  */

  use RegistersUsers;

  /**
   * Where to redirect users after registration.
   *
   * @var string
   */
  protected $redirectTo = '/home';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('guest');
  }

  /**
   * Upload file
   */
  public function uploadSubmit(Request $request)
  {
    $avatar = $request['photo'];
    // $filename = $avatar->store('photos');
    // $image = Image::create([
    //   'user_id' => $user_id,
    //   'filename' => $filename
    // ]);
    // return $image->getFilename();

    if ($request->hasFile('photo')) {
      $file = $request->photo;
      $path = $request->photo->store('public');
    }
    return response()->json(['path' => str_replace('public', 'storage', $path)]);
  }

  /**
   * Execute the validation and create user
   */
  protected function excuteRegistrationUser (Request $request)
  {
      return $this->validator($request->toArray());
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    $val = Validator::make($data, [
      'username' => 'required|string|min:4|max:24|unique:users',
      'firstname' => 'required|string|min:4|max:24',
      'lastname' => 'required|string|min:4|max:24',
      'email' => 'required|string|email|min:4|max:100|unique:users',
      'password' => 'required|string|min:6|confirmed',
      'password_confirmation' => 'required|same:password',
    ]);

    // If validator correct create user
    if ($val->fails()) {
      return response()->json(['errors'=>$val->errors()]);
    }else{
      return $this->create($data);
    }
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return User
   */
  protected function create(array $data)
  {
    $user = User::create([
      'username' => $data['username'],
      'firstname' => $data['firstname'],
      'lastname' => $data['lastname'],
      'email' => $data['email'],
      'password' => bcrypt($data['password']),
      'api_token' => str_random(60),
      'team_private' => str_random(60),
      'avatar' => $data['avatar']
    ]);

    /**
     * After creating the user
     * Create newsfeed
     */
    $newsfeed = Newsfeed::create([
      'name' => 'NewsCrush\'s '.$data['username'],
      'user_id' => $user->getId(),
    ]);

    /**
     * Create Action for user
     */
    $action = Action::create([
      'badges' => ini_gamification::initGamification(),
      'user_id' => $user->getId(),
    ]);

    /*
     * Add newsfeed to the user
     * And save it
     */
    $user->addNewfeed($newsfeed->getId());
    $user->save();

    $user = addUserParams::addUserParams($user, $action);

    /**
     * Log the user after registration
     */
    $credentials = ['email' => $data['email'], 'password' => $data['password']];
    $token = null;
    try {
     if (!$token = JWTAuth::attempt($credentials)) {
      return response()->json(['invalid_email_or_password'], 422);
     }
    } catch (JWTAuthException $e) {
        return response()->json(['failed_to_create_token'], 500);
    }

    return response()->json(array_merge(compact('token'), ['newsfeed' => $newsfeed->getId(), 'user' => $user]));
  }
}
