<?php

namespace App\Http\Controllers;

use App\News;
use App\User;
use App\Source;
use JWTAuth;
use Illuminate\Http\Request;

class NewsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    //
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
      //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }


   /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function onenews(Request $request)
  {
    $news_id = intval($request['news_id']);
    $news = News::where('id', $news_id)->first();

    $source = Source::find($news->source_id);
    $news->source = $source;
    $author = User::find($news->user_id);
    $author = User::find($news->user_id);
    $news->author = $author;

    return response()->json(['result' => $news]);
  }

  public function newsR(Request $request)
  {
    $news_id = intval($request['news_id']);
    $news = News::where('id', $news_id)->first();

    $source = Source::find($news->source_id);
    $news->source = $source;
    $author = User::find($news->user_id);
    $author = User::find($news->user_id);
    $news->author = $author;

    return response()->json(['result' => $news]);
  }

   /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function news(Request $request)
  {
    $newsfeed_id = intval($request['newsfeed_id']);
    $team_id = intval($request['team_id']);
    $news = News::where('newsfeed_id', $newsfeed_id)->get();
    $res = [];

    // For all the news add source
    foreach ($news as $n) {
      $source = Source::find($n->source_id);
      $n->source = $source;
      $author = User::find($n->user_id);
      $n->author = $author;
    }

    if($team_id){
      foreach ($news as $n) {
        if($n->isInTeams($team_id)){
          array_push($res, $n);
        }
      }
      return response()->json(['result' => $res]);
    }
    return response()->json(['result' => $news]);
  }

  /**
   * Return one specific news
   * check news exist
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function checknewsinprivate(Request $request)
  {
    $user = JWTAuth::toUser($request->token);
    $newsfeed_id = intval($request['newsfeed_id']);
    $team_id = intval($request['team_id']);
    $url = $request['url'];
    $news = News::where('newsfeed_id', $newsfeed_id)
      ->where('url', $url)
      ->whereIn('team_ids', [$team_id])
      ->find($user->getId());
      // ->first();

    $exist = $news? true: false;
    return response()->json(['result' => $exist]);
  }

  /**
   * Return one specific news
   * check news exist
   * @param Request $request
   * @return \Illuminate\Http\Response
   */
  public function checknewsinnewsfeed(Request $request)
  {
    $user = JWTAuth::toUser($request->token);
    $newsfeed_id = intval($request['newsfeed_id']);
    $team_id = intval($request['team_id']);
    $url = $request['url'];
    $news = News::where('newsfeed_id', $newsfeed_id)
      ->where('url', $url)
      ->whereNotIn('team_ids', [$team_id])
      ->find($user->getId());
      // ->first();
    $exist = $news? true: false;
    return response()->json(['result' => $exist]);
  }

  /**
   * Post news
   */
  public function post(Request $request)
  {
    $user = JWTAuth::toUser($request->token);
    $param = $request['param'];

    $news = new News();
    $news->setPublishedAt($param['published']);
    $news->setMedia($param['media']);
    $news->setImages($param['images']);
    $news->setCommentsCount(0);
    // $news->addViewer([]); // No need
    $news->setAvgRate(0.0);
    // $news->addVote([]); // No need
    $news->setDescription($param['description']);
    $news->setUrl($param['url']);
    $news->setTitle($param['title']);
    $news->setOriginalText($param['original_text']);
    $news->setHighlightedText($param['highlighted_text']);
    // $news->addTag([]); // No need
    $news->setTagsCount(0);
    // $news->addTeam([]); // No need
    $news->user_id = $user->getId();
    $news->newsfeed_id = $param['newsfeed_id'];
    $news->source_id = $param['source_id'];
    $news->save();

    // $news = News::create([
    //   'published_at' => $param['published'],
    //   'media' => $param['media'],
    //   'images' => $param['images'],
    //   'comments_count' => 0,
    //   'views' => [],
    //   'avg_rate' => 0.0,
    //   'votes' => [],
    //   'description' => $param['description'],
    //   'url' => $param['url'],
    //   'title' => $param['title'],
    //   'original_text' => $param['original_text'],
    //   'highlighted_text' => $param['highlighted_text'],
    //   'tags' => $param['tags'],
    //   'tags_count' => 0,
    //   'team_ids' => $param['tags_team'],
    //   'user_id' => $user->getId(),
    //   'newsfeed_id' => $param['newsfeed_id'],
    //   'source_id' => $param['source_id'],
    // ]);
    return response()->json(['result' => true]);
  }
}
