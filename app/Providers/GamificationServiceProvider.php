<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GamificationServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
    //
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Create json gamification
   */
  public static function initJsonGamification() {
    return serialize([
      'badges' => [
        [
          "name" => "comment",
          "points" => [
            "default" => 1
          ],
          "medals" => [
            [
              "points" => 8,
              "type" => "bronze",
              "image" => "comment_bronze.png",
              "text" => "commenting number articles",
              "badge" => "Commenter"
            ],
            [
              "points" => 12,
              "type" => "silver",
              "image" => "comment_silver.png",
              "text" => "commenting number articles",
              "badge" => "Commenter"
            ],
            [
              "points" => 18,
              "type" => "gold",
              "image" => "comment_gold.png",
              "text" => "commenting number articles",
              "badge" => "Commenter"
            ]
          ]
        ],
        [
          "name" => "connexion",
          "rules" => [
            "weekly" => 3
          ],
          "points" => [
            "default" => 1,
            "weekly" => 3
          ],
          "medals" => [
            [
              "points" => 3,
              "type" => "bronze",
              "image" => "connexion_bronze.png",
              "text" => "log in number times",
              "badge" => "Logger"
            ],
            [
              "points" => 8,
              "type" => "silver",
              "image" => "connexion_silver.png",
              "text" => "log in number times",
              "badge" => "Logger"
            ],
            [
              "points" => 12,
              "type" => "gold",
              "image" => "connexion_gold.png",
              "text" => "log in number times",
              "badge" => "Logger"
            ]
          ]
        ],
        [
          "name" => "expert",
          "rules" => [
            "level1" => 3,
            "level2" => 7,
            "level3" => 15
          ],
          "points" => [
            "default" => 2
          ],
          "medals" => [
            [
              "points" => 6,
              "type" => "bronze",
              "image" => "expert_bronze.png",
              "text" => "experting number times",
              "badge" => "Expert"
            ],
            [
              "points" => 10,
              "type" => "silver",
              "image" => "expert_silver.png",
              "text" => "experting number times",
              "badge" => "Expert"
            ],
            [
              "points" => 16,
              "type" => "gold",
              "image" => "expert_gold.png",
              "text" => "experting number times",
              "badge" => "Expert"
            ]
          ]
        ],
        [
          "name" => "reader",
          "points" => [
            "default" => 1
          ],
          "medals" => [
            [
              "points" => 4,
              "type" => "bronze",
              "image" => "read_bronze.png",
              "text" => "reading number articles",
              "badge" => "Reader"
            ],
            [
              "points" => 8,
              "type" => "silver",
              "image" => "read_silver.png",
              "text" => "reading number articles",
              "badge" => "Reader"
            ],
            [
              "points" => 12,
              "type" => "gold",
              "image" => "read_gold.png",
              "text" => "reading number articles",
              "badge" => "Reader"
            ]
          ]
        ],
        [
          "name" => "share",
          "points" => [
            "default" => 7
          ],
          "medals" => [
            [
              "points" => 4,
              "type" => "bronze",
              "image" => "share_bronze.png",
              "text" => "sharing number news",
              "badge" => "Sharer"
            ],
            [
              "points" => 8,
              "type" => "silver",
              "image" => "share_silver.png",
              "text" => "sharing number news",
              "badge" => "Sharer"
            ],
            [
              "points" => 12,
              "type" => "gold",
              "image" => "share_gold.png",
              "text" => "sharing number news",
              "badge" => "Sharer"
            ]
          ]
        ],
        [
          "name" => "relevant",
          "points" => [
            "default" => 2
          ],
          "medals" => [
            [
              "points" => 4,
              "type" => "bronze",
              "image" => "relevant_bronze.png",
              "text" => "rating number news",
              "badge" => "Rater"
            ],
            [
              "points" => 8,
              "type" => "silver",
              "image" => "relevant_silver.png",
              "text" => "rating number news",
              "badge" => "Rater"
            ],
            [
              "points" => 12,
              "type" => "gold",
              "image" => "relevant_gold.png",
              "text" => "rating number news",
              "badge" => "Rater"
            ]
          ]
        ],
        [
          "name" => "post",
          "points" => [
            "default" => 3,
            "tags" => 1,
            "description" => 1,
            "HL" => 1
          ],
          "medals" => [
            [
              "points" => 4,
              "type" => "bronze",
              "image" => "post_bronze.png",
              "text" => "posting number articles",
              "badge" => "Poster"
            ],
            [
              "points" => 8,
              "type" => "silver",
              "image" => "post_silver.png",
              "text" => "posting number articles",
              "badge" => "Poster"
            ],
            [
              "points" => 12,
              "type" => "gold",
              "image" => "post_gold.png",
              "text" => "posting number articles",
              "badge" => "Poster"
            ]
          ]
        ]
      ],
      'enable' => 1,
      'levels' => [
        [
          "level" => 0,
          "coeficient" => 1,
          "points" => 4,
          "objectif" => "You can earn Badges as well"
        ],
        [
          "level" => 1,
          "coeficient" => 1,
          "points" => 14,
          "objectif" => "Invite teammate to share knowledge"
        ],
        [
          "level" => 2,
          "coeficient" => 1,
          "points" => 26,
          "objectif" => "Connect every day to have extra points"
        ],
        [
          "level" => 3,
          "coeficient" => 1.1,
          "points" => 41,
          "objectif" => "Click on the tag #help to know more about extra features on newscrush"
        ],
        [
          "level" => 4,
          "coeficient" => 1.1,
          "points" => 58,
          "objectif" => "Create a newsletter is a good way to share your business watch"
        ],
        [
          "level" => 5,
          "coeficient" => 1.1,
          "points" => 78,
          "objectif" => "Click on the tag #help to access extra feature on newscrush"
        ],
        [
          "level" => 6,
          "coeficient" => 1.1,
          "points" => 93,
          "objectif" => "Go in your profile to know how many points you earn"
        ],
        [
          "level" => 7,
          "coeficient" => 1.1,
          "points" => 113,
          "objectif" => "Wacht out your teammate to know their level"
        ],
        [
          "level" => 8,
          "coeficient" => 1.2,
          "points" => 134,
          "objectif" => "How far you can go?"
        ],
        [
          "level" => 9,
          "coeficient" => 1.2,
          "points" => 156,
          "objectif" => "Who is the best newscrusher of your team?"
        ],
        [
          "level" => 10,
          "coeficient" => 1.2,
          "points" => 179,
          "objectif" => "send an email to sara@newscrush.io to win a coffee mug"
        ],
        [
          "level" => 11,
          "coeficient" => 1.2,
          "points" => 203,
          "objectif" => "Go in your profile to know how many points you earn"
        ],
        [
          "level" => 12,
          "coeficient" => 1.3,
          "points" => 228,
          "objectif" => "In newscrush, you can share articles directly on social network"
        ],
        [
          "level" => 13,
          "coeficient" => 1.3,
          "points" => 254,
          "objectif" => "Create mailing list to send newsletter"
        ],
        [
          "level" => 14,
          "coeficient" => 1.3,
          "points" => 3000000,
          "objectif" => "The princess is in another castle"
        ]
      ]
    ]);
  }

  /**
   * Init gamification field for user
   *
   * @return void
   */
  public static function initGamification()
  {
    return serialize([
      'comment' => [
        'count' => 0
      ],
      'connexion' => [
        'count' => 0,
        'weekly' => 0
      ],
      'expert' => [
        'count' => 0
      ],
      'reader' => [
        'count' => 0
      ],
      'share' => [
        'count' => 0
      ],
      'relevant' => [
        'count' => 0
      ],
      'post' => [
        'count' => 0,
        'tags' => 0,
        'description' => 0,
        'HL' => 0
      ],
    ]);
  }
}
