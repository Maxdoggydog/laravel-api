<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AddUserParamsServiceProvider extends ServiceProvider
{
  /**
   * Add params to the user
   */
  public static function addUserParams ($user, $action) {
    $action->badges = $action->getBadges();
    $user->gamification = $action;
    $user->newsfeed_ids = $user->getNewfeed();
    $user->team_ids = $user->getTeams();
    return $user;
  }
}