<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model {

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'teams';

  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'created_at' => 'datetime',
    'updated_at' => 'datetime',
    'name' => 'string',
    'tags_allowed' => 'array',
    'tags_blocked' => 'boolean',
    'users' => 'array',
    'futur_members' => 'array',
  ];

  /**
   * Get the news.
   */
  public function user () {
    return $this->belongsTo('App\User');
  }

  /**
   * Get the newsfeed
   */
  public function newsfeeds () {
    return $this->belongsToMany('App\Newsfeed', 'foreign_key');
  }
}
