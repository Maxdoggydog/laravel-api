<?php

namespace App;

use Laravel\Passport\HasApiTokens; // For OAuth2
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;

class User extends Authenticatable
{
  use HasApiTokens, Notifiable;

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'users';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'firstname',
      'lastname',
      'email',
      'username',
      'email_verified',
      'first_connexion',
      'lang',
      'avatar',
      'api_token',
      'password',
      'newsfeed_ids' => 'array',
      'team_ids' => 'array',
      'team_private',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'password', 'remember_token', 'api_token',
  ];

  /**
   * Foreign key to get gamification || table action
   */
  public function gamifcation () {
    return $this->hasOne('App\Action', 'user_id');
  }

  /**
   * Foreign key to get Activity
   */
  public function activities () {
    return $this->hasOne('App\Activity', 'foreign_key');
  }

  /**
   * Foreign key to get team
   */
  public function teams () {
    return $this->hasMany('App\Team', 'foreign_key');
  }

  /**
   * Foreign key to get newsfeed
   */
  public function newsfeed () {
    return $this->hasOne('App\Newsfeed', 'user_id');
  }

  /**
   * Foreign key to get mailing
   */
  public function mailings () {
    return $this->hasMany('App\Maiing', 'foreign_key');
  }

  /**
   * Foreign key to get news
   */
  public function news () {
    return $this->hasMany('App\News', 'user_id');
  }

  /**
   * Foreign key to get tuto
   */
  public function tutos () {
    return $this->hasMany('App\Tuto', 'foreign_key');
  }

  ///////////////  Methods for Model  /////////////////

  /**
   * Return user id
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Get newsfeed
   */
  public function getNewfeed () {
    return json_decode($this->newsfeed_ids);
  }

  /**
   * Add newsfeed id in user field
   */
  public function addNewfeed ($newsfeed_id) {
    $array = collect($this->newsfeed_ids);
    $array->prepend($newsfeed_id);
    $this->newsfeed_ids = $array;
  }

  /**
   * Check if id in newsfeeds
   */
  public function isInNewfeeds ($newsfeed_id) {
    return in_array($newsfeed_id, $this->getNewfeed());
  }

  /**
   * Set avatar path
   */
  public function setAvatar ($path) {
    $this->avatar = $path;
  }

  /**
   * Get avatar path
   */
  public function getAvatar () {
    return $this->avatar;
  }

  /**
   * Add Team to Teams
   */
  public function addTeam ($team_id) {
    $array = collect($this->team_ids);
    $array->prepend($team_id);
    $this->team_ids = $array;
  }

  /**
   * Check if id in teams
   */
  public function isInTeams ($team_id) {
    return in_array($team_id, $this->getTeams());
  }

  /**
   * Get teams
   */
  public function getTeams () {
    return json_decode($this->team_ids);
  }

  /**
   * Get private team
   */
  public function getPrivateTeam () {
    return $this->team_private;
  }
}
