<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datalogs extends Model {

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'datalogs';
  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'type' => 'object',
    'created_at' => 'datetime',
    'action' => 'string',
    'value' => 'object',
  ];

  /**
   * Get the user that owns this User.
   */
  public function user () {
    return $this->belongsTo('App\User');
  }

  /**
   * Get the user that owns this Team.
   */
  public function team () {
    return $this->belongsTo('App\Team');
  }

  /**
   * Get the user that owns this newsfeed.
   */
  public function newsfeed () {
    return $this->belongsTo('App\Newsfeed');
  }
}
