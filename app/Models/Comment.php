<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'comments';
  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'content' => 'string',
    'created_at' => 'datetime',
  ];

  /**
   * Get the user that owns this News.
   */
  public function news () {
    return $this->belongsTo('App\News');
  }

  /**
   * Get the user that owns this User.
   */
  public function user () {
    return $this->belongsTo('App\User');
  }
}
