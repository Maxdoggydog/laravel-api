<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'notifications';
  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'created_at' => 'datetime',
    'sent_at' => 'datetime',
    'viewed_at' => 'datetime',
  ];

  /**
   * Get the user that owns this User.
   */
  public function user () {
    return $this->belongsTo('App\User');
  }

  /**
   * Get the teams
   */
  public function teams () {
    return $this->belongsTo('App\Activity', 'foreign_key');
  }
}
