<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model {

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'activities';
  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'type' => 'object',
    'created_at' => 'datetime',
    'target_user' => 'integer',
  ];

  /**
   * Get the user that owns this User.
   */
  public function user () {
    return $this->belongsTo('App\User');
  }

  /**
   * Get the user that owns this Team.
   */
  public function team () {
    return $this->belongsTo('App\Team');
  }

  /**
   * Get the user that owns this newsfeed.
   */
  public function newsfeed () {
    return $this->belongsTo('App\Newsfeed');
  }
}
