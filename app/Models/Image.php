<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'images';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'user_id'
    'filename',
  ];

  /**
   * Foreign key to get user
   */
  public function user() {
    return $this->belongsTo('App\User', 'user_id');
  }

  /**
   * Get the filename
   */
  public function getFilename() {
    return $this->filename;
  }

  /**
   * Set the filename
   */
  public function setFilename($filename) {
    $this->filename = $filename;
  }
}
