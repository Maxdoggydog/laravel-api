<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gamification extends Model
{
  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'gamifications';

  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'json'
  ];

  /**
   * Set json
   */
  public function setJson ($json) {
    return $this->json = serialize($json);
  }

  /**
   * Get json
   */
  public function getJson () {
    return unserialize($this->json);
  }
}
