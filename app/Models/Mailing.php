<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mailing extends Model {

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'mailings';
  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'created_at' => 'datetime',
    'name' => 'string',
    'datas' => 'array',
  ];

  /**
   * Get the user that owns this User.
   */
  public function user () {
    return $this->belongsTo('App\User');
  }
}
