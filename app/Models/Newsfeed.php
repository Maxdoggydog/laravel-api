<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsfeed extends Model {

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'newsfeeds';
  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'user_id',
    'teams',
  ];

  /**
   * Get the user that owns this User.
   */
  public function user () {
    return $this->belongsTo('App\User');
  }

  /**
   * Get the teams
   */
  public function teams () {
    return $this->belongsToMany('App\Team', 'foreign_key');
  }

  /**
   * Foreign key to get news
   */
  public function news () {
    return $this->hasMany('App\News', 'newsfeed_id');
  }

  /**
   * Return newsfeed id
   */
  public function getId() {
    return $this->id;
  }
}
