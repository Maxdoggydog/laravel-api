<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Action extends Model {


  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'actions';
  protected $primaryKey = 'user_id';

  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'user_id',
    'badges', // use serialize to store it ==> son need unserialize to get it || change 'gamification' to 'badges'
    'activities' => 'object', // TODO : Not sur for this one
  ];

  /**
   * Get the user that owns this Action.
   */
  public function user () {
    return $this->belongsTo('App\User');
  }

  /**
   * Get gamification
   */
  public function getBadges () {
    return unserialize($this->badges);
  }

  /**
   * Set gamification
   */
  public function setBadges ($badges) {
    $this->badges = serialize($badges);
  }
}
