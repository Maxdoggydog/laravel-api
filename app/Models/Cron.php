<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cron extends Model {

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'crons';
  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'done' => 'boolean',
    'created_at' => 'datetime',
    'updated' => 'datetime',
    'params' => 'object',
    'name' => 'string',
  ];

  /**
   * Get the user that owns this Newsfeed.
   */
  public function newsfeed () {
    return $this->belongsTo('App\Newsfeed');
  }
}
