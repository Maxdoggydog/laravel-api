<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tuto extends Model {

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'tutos';

  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'created_at' => 'datetime',
    'order' => 'integer',
    'lang' => 'string',
    'title' => 'string',
    'description' => 'string',
    'content' => 'string',
  ];

  /**
   * Get the news.
   */
  public function user () {
    return $this->belongsTo('App\User');
  }
}
