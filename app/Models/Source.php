<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'sources';
  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'created_at' => 'datetime',
    'sent_at' => 'datetime',
    'viewed_at' => 'datetime',
    'url',
    'name',
    'logo',
  ];

  /**
   * Get the news.
   */
  public function news () {
    return $this->belongsToMany('App\News', 'source_id');
  }

  /**
   * Get the newsfeed
   */
  public function newsfeeds () {
    return $this->belongsToMany('App\Newsfeed');
  }

  /**
   * Get source id
   */
  public function getId()
  {
    return $this->id;
  }
}
