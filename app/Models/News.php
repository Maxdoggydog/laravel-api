<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model {

  /**
   * The table associated with the model.
   * @var string
   */
  protected $table = 'news';
  protected $primaryKey = 'user_id';

  /**
   * The attributes that are mass assignable
   *
   * @var array
   */
  protected $fillable = [
    'user_id',
    'newsfeed_id',
    'source_id',
    // 'comment_id',
    'created_at' => 'datetime',
    'updated_at' => 'datetime',
    'published_at' => 'datetime',
    'media',
    'images',
    'comments_count' => 'integer',
    'views',
    'avg_rate' => 'float',
    'votes',
    'description' => 'string',
    'url' => 'string',
    'title' => 'string',
    'original_text' => 'string',
    'highlighted_text' => 'string',
    'tags',
    'tags_count' => 'integer',
    'team_ids',
  ];

  /**
   * Get the user that owns this User.
   */
  public function user () {
    return $this->belongsTo('App\User');
  }

  /**
   * Get the user that owns this newsfeed.
   */
  public function newsfeed () {
    return $this->belongsTo('App\Newsfeed');
  }

  /**
   * Get the source
   */
  public function source () {
    return $this->belongsToMany('App\Source');
  }

  /**
   * Get the comment
   */
  public function comments () {
    return $this->hasMany('App\Comment', 'news_id');
  }

  /**
   * Get the teams
   */
  public function teams () {
    return $this->belongsToMany('App\Team', 'foreign_key');
  }

  ///////////////////////// Parameters /////////////////////////

  /**
   * Return news id
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set tags count
   */
  public function setTagsCount($count) {
    $this->tags_count = $count;
  }

  /**
   * Get tags count
   */
  public function getTagsCount() {
    return $this->tags_count;
  }

  /**
   * Set comments count
   */
  public function setCommentsCount($count) {
    $this->comments_count = $count;
  }

  /**
   * Get comments count
   */
  public function getCommentsCount() {
    return $this->comments_count;
  }

  /**
   * Set avg rate
   */
  public function setAvgRate($rate) {
    $this->avg_rate = $rate;
  }

  /**
   * Get avg rate
   */
  public function getAvgRate() {
    return $this->avg_rate;
  }

  /**
   * Set description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * Get description
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * Set url
   */
  public function setUrl($url) {
    $this->url = $url;
  }

  /**
   * Get url
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * Set title
   */
  public function setTitle($title) {
    $this->title = $title;
  }

  /**
   * Get title
   */
  public function getTitle() {
    return $this->title;
  }

  /**
   * Set original text
   */
  public function setOriginalText($original_text) {
    $this->original_text = $original_text;
  }

  /**
   * Get original text
   */
  public function getOriginalText() {
    return $this->original_text;
  }

  /**
   * Set highlighted text
   */
  public function setHighlightedText($highlighted_text) {
    $this->highlighted_text = $highlighted_text;
  }

  /**
   * Get highlighted text
   */
  public function getHighlightedText() {
    return $this->highlighted_text;
  }

  /**
   * Set published at
   */
  public function setPublishedAt($published_at) {
    $this->published_at = $published_at;
  }

  /**
   * Get published at
   */
  public function getPublishedAt() {
    return $this->published_at;
  }

  /**
   * Add Team to Teams
   */
  public function addTeam ($team_id) {
    $array = collect($this->team_ids);
    $array->prepend($team_id);
    $this->team_ids = $array;
  }

  /**
   * Check if id in teams
   */
  public function isInTeams ($team_id) {
    return in_array($team_id, $this->getTeams());
  }

  /**
   * Get teams
   */
  public function getTeams () {
    // If the $this->team_ids == null => return empty array
    return $this->team_ids ? json_decode($this->team_ids) : [];
  }

  /**
   * Add Tag to Tags
   */
  public function addTag ($tag) {
    $array = collect($this->tags);
    $array->prepend($tag);
    $this->tags = $array;
  }

  /**
   * Check if tag in tags
   */
  public function isInTags ($tag) {
    return in_array($tag, $this->getTags());
  }

  /**
   * Get tags
   */
  public function getTags () {
    return json_decode($this->team_ids);
  }

  /**
   * Add viewer
   */
  public function addViewer ($viewer) {
    $array = collect($this->views);
    $array->prepend($viewer);
    $this->views = $array;
  }

  /**
   * Check if viewer in views
   */
  public function isInViews ($viewer) {
    return in_array($viewer, $this->getViews());
  }

  /**
   * Get views
   */
  public function getViews () {
    return json_decode($this->views);
  }

  /**
   * Add vote
   */
  public function addVote ($vote) {
    $array = collect($this->votes);
    $array->prepend($vote);
    $this->votes = $array;
  }

  /**
   * Check if vote in votes
   */
  public function isInVotes ($vote) {
    return in_array($vote, $this->getVotes());
  }

  /**
   * Get votes
   */
  public function getVotes () {
    return json_decode($this->votes);
  }

  /**
   * Set images
   */
  public function setImages ($image) {
    $this->images = json_encode($this->image);
  }

  /**
   * Get images
   */
  public function getImages () {
    return json_decode($this->images);
  }

  /**
   * Set media
   */
  public function setMedia ($media) {
    $this->media = json_encode($this->media);
  }

  /**
   * Get media
   */
  public function getMedia () {
    return json_decode($this->media);
  }

  /**
   * Get souce id
   */
  public function getSourceId()
  {
    return $this->source_id;
  }

}
