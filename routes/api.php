<?php

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/test', function (Request $request) {
    return response()->json(['id' => 'maxi', 'birthday' => '01/08/1989']);
});

Route::post('/test/post', function (Request $request) {
    return response()->json($request);
});

/**
 * Upload file picture
 */
Route::post('/uploadpicture', 'Auth\RegisterController@uploadSubmit');

/**
 * Check the username already exist
 */
Route::post('/checkusername', function (Request $request) {
  if (User::where('username', '=', $request['username'])->exists()) {
    return response()->json(['error' => true]);
  }
  return response()->json(['error' => false]);
});

/**
 * Check the email already exist
 */
Route::post('/checkemail', function (Request $request) {
  if (User::where('email', '=', $request['email'])->exists()) {
    return response()->json(['error' => true]);
  }
  return response()->json(['error' => false]);
});

/**
 * Here is the path for new user
 * Create the user here
 */
Route::post('/createuser', 'Auth\RegisterController@excuteRegistrationUser');

/**
 * Log the user to the platform
 */
Route::post('/login', 'Auth\LoginController@loginUser');

/**
 * With the token return
 * Connect the user
 */
Route::group(['middleware' => 'jwt.auth'], function () {
  // Get user information
  Route::post('/user', 'UserController@getAuthUser');
  // Get user gamification
  Route::post('/getgamification', 'ActionController@get');
  // Get json
  Route::post('/getjson', 'GamificationController@get');
  // Logout user
  Route::get('/logout', 'Auth\LoginController@logout');
  // Get newsfeed user
  Route::post('/getnewsfeeduser', 'UserController@getnewsfeeduser');
  // Check is the newsfeed belong to the user
  Route::post('/newsfeedbelongstouser', 'UserController@newsfeedbelongstouser');
  // Get all user from newsfeed or team
  Route::post('/getusers', 'UserController@users');
  // Get all news from newsfeed or team
  Route::post('/getnews', 'NewsController@news');
  // Check if news exist in bdd
  Route::post('/checknewsinprivate', 'NewsController@checknewsinprivate');
  // Check if news exist in bdd
  Route::post('/checknewsinnewsfeed', 'NewsController@checknewsinnewsfeed');
  // Check if source exit
  Route::post('/checksource', 'SourceController@getorcreate');
  // Post
  Route::post('/post', 'NewsController@post');
  // Get one news
  Route::post('/onenews', 'NewsController@onenews');
});

// Get one news
Route::get('/news/{news_id}', 'NewsController@newsR');