<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContraintsToNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      /**
       * If the table 'newsfeeds' 'sources' and 'users' exist
       * Add the contraint
       * '::table' => update table
       */
      // if (Schema::hasTable('users') && Schema::hasTable('newsfeeds') && Schema::hasTable('sources') && Schema::hasTable('comments')) {
        Schema::table('news', function(Blueprint $table) {
          echo 'I will do the foreign key';
          $table->foreign('user_id')->references('id')->on('users');
          $table->foreign('newsfeed_id')->references('id')->on('newsfeeds');
          $table->foreign('source_id')->references('id')->on('sources');
          // $table->foreign('comment_id')->references('id')->on('comments');
          echo 'Foreign key did';
        });
      // }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
