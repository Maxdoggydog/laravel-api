<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('news', function (Blueprint $table) {
      $table->engine = 'InnoDB';
      $table->increments('id');
      $table->float('avg_rate')->default(0.0);
      $table->text('highlighted_text')->nullable();
      $table->text('original_text')->nullable();
      $table->string('title');
      $table->string('url');
      $table->text('description')->nullable();
      $table->integer('tags_count')->default(0);
      $table->text('media');
      $table->integer('comments_count')->default(0);
      // $table->integer('comment_id')->unsigned();
      $table->text('images');
      $table->text('views');
      $table->text('votes');
      $table->text('tags');
      $table->string('team_ids');
      $table->datetime('published_at')->nullable();
      $table->integer('user_id')->unsigned();
      $table->integer('newsfeed_id')->unsigned();
      $table->integer('source_id')->unsigned();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('news');
  }
}
